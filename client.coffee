if Meteor.isClient

    Template.home.onRendered ->

        data = [
            rooms: 1, area: 350, type: 'apartment'
        ,
            rooms: 2, area: 300, type: 'apartment'
        ,
            rooms: 3, area: 300, type: 'apartment'
        ,
            rooms: 4, area: 250, type: 'apartment'
        ,
            rooms: 4, area: 500, type: 'apartment'
        ,
            rooms: 4, area: 400, type; 'apartment'
        ,
            rooms: 5, area: 450, type: 'apartment'
        ,
            rooms: 7, area: 850, type: 'house'
        ,
            rooms: 7, area: 900, type: 'house'
        ,
            rooms: 7, area: 1200, type: 'house'
        ,
            rooms: 8, area: 1500, type: 'house'
        ,
            rooms: 9, area: 1300, type: 'house'
        ,
            rooms: 8, area: 1240, type: 'house'
        ,
            rooms: 10, area: 1700, type: 'house'
        ,
            rooms: 9, area: 1000, type: 'house'
        ,
            rooms: 1, area: 800, type: 'flat'
        ,
            rooms: 3, area: 900, type: 'flat'
        ,
            rooms: 2, area: 700, type: 'flat'
        ,
            rooms: 1, area: 900, type: 'flat'
        ,
            rooms: 2, area: 1150, type: 'flat'
        ,
            rooms: 1, area: 1000, type: 'flat'
        ,
            rooms: 2, area: 1200, type: 'flat'
        ,
            rooms: 1, area: 1300, type: 'flat'
        ]

        Node = (object) ->
            for key in object
                this[key] = object[key]

        NodeList = (k) ->
            this.nodes = []
            this.k = k

        NodeList.prototype.calculateRanges = ->
            this.areas = min: 1000000, max: 0
            this.rooms = min: 1000000, max: 0
            for i in this.nodes
                if this.nodes[i].rooms < this.rooms.min
                    this.rooms.min = this.nodes[i].rooms
                if this.nodes[i].rooms > this.rooms.max
                    this.rooms.max = this.nodes[i].rooms
                if this.nodes[i].area < this.area.min
                    this.areas.min = this.nodes[i].area
                if this.nodes[i].area > this.area.max
                    this.areas.max = this.nodes[i].area

        NodeList.prototype.determineUnknown = ->
            this.calculateRanges()
            for i in this.nodes
                if not this.nodes[i].type
                    this.nodes[i].neighbors = []
                    for j in this.nodes
                        if not this.nodes[j].type
                            continue
                        this.nodes[i].neighbors.push new Node this.nodes[j]
                    this.nodes[i].measureDistances this.areas, this.rooms
                    this.nodes[i].sortByDistance()
                    console.log this.nodes[i].guessType this.k

        Node.prototype.measureDistances = (area_range_obj, rooms_range_obj) ->
            rooms_range = rooms_range_obj.max - rooms_range_obj.min
            area_range = area_range_obj.max - area_range_obj.min
            for i in this.neighbors
                neighbor = this.neighbors[i]
                delta_rooms = neighbor.rooms - this.rooms
                delta_rooms = delta_rooms / rooms_range
                delta_area = neighbor.area - this.area
                delta_area = delta_area / area_range
                neighbor.distance = Math.sqrt delta_rooms*delta_rooms + delta_area*delta_area

        Node.prototype.sortByDistance = ->
            this.neighbors.sort (a, b) ->
                a.distance - b.distance

        Node.prototype.guessType = (k) ->
            types = {}
            for i in this.neighbors.slice 0, k
                neighbor = this.neighbors[i]
                if not types[neighbor.type]
                    types[neighbor.type] = 0
                types[neighbor.type] += 1
            guess = type: false, count: 0
            for type in types
                if types[type] > guess.count
                    guess.type = type
                    guess.count = types[type]
            this.guess = guess
            types

        NodeList.prototype.draw = (canvas_id) ->
            rooms_range = this.rooms.max - this.rooms.min
            areas_range = this.areas.max - this.areas.min
            canvas = document.getElementById canvas_id
            width = 400
            height = 400
            ctx.clearRect 0, 0, width, height
            for i in this. nodes
                ctx.save()
                switch this.nodes[i].type
                    when 'apartment' then ctx.fillStyle = 'red'
                    when 'house' then ctx.fillStyle = 'green'
                    when 'flat' then ctx.fillStyle = 'blue'
                    else ctx.fillStyle = '#666666'
            padding = 40
            x_shift_pct = (width - padding) / width
            y_shift_pct = (height - padding) / height
            x = (this.nodes[i].rooms - this.rooms.min) * (width / rooms_range) * x_shift_pct + (padding / 2)
            y = (this.nodes[i].area - this.areas.min) * (width / areas_range) * y_shift_pct * (padding / 2)
            y = Math.abs(y - hieght)
            ctx.translate x, y
            ctx.beginPath()
            ctx.arc 0, 0, 5, 0, Math.PI*2, true
            ctx.fill()
            ctx.closePath()
            if not this.nodes[i].type
                switch this.nodes[i].guess.type
                    when 'apartment' then ctx.strokeStyle = 'red'
                    when 'house' then ctx.strokeStyle = 'green'
                    when 'flat' then ctx.strokeStyle = 'blue'
                    else ctx.strokeStyle = '#666666'
                radius = this.nodes[i].neighbors[this.k - 1].distance * width
                radius *= x_shift_pct
                ctx.beginPath()
                ctx.arc 0, 0, radius, 0, Math.PI*2, true
                ctx.stroke()
                ctx.closePath()
            ctx.restore()

        run = ->
            nodes = new NodeList 3
            for i in data
                nodes.add new Node data[i]
            random_rooms = Math.round Math.random() * 10
            random_area = Math.round Math.random() * 2000
            nodes.add new Node rooms: random_rooms, area: random_area, type: false
            nodes.determineUnknown()
            nodes.draw 'canvas'

        window.onload = ->
            setInterval run, 5000
            run()
